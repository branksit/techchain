-- ----------------------------------------------------------------------------------
-- 
-- CRIANDO O BANCO DE DADOS `techchain`
--
-- BANCO DE DADOS RESPONSAVEL POR ARMAZENAR AS INFORMACOES CONTIDAS NA APLICACAO
CREATE DATABASE `techchain`;

-- ----------------------------------------------------------------------------------
-- 
-- CRIANDO A TABELA `usuario`
--
-- REPONSAVEL POR ARMAZENAR AS INFORMACOES DOS USUARIOS
CREATE TABLE `usuario`
(
	`id` BIGINT AUTO_INCREMENT,
	`nome_completo` VARCHAR(355) NOT NULL,
	`data_nascimento` DATE NOT NULL,
	`email` VARCHAR(355) NOT NULL,
	`senha` VARCHAR(80) NOT NULL,
	`rg` CHAR(12) NULL,
	`cpf` CHAR(14) NULL,
	`estado_civil` VARCHAR(150) NULL,
	`cep` CHAR(9) NULL,
	`uf` CHAR(2) NULL,
	`cidade` VARCHAR(355) NULL,
	`bairro` VARCHAR(355) NULL,
	`endereco` VARCHAR(355) NULL,
	`numero` INT NULL,
	`complemento` VARCHAR(355) NULL,
	`status` INT NULL,
	PRIMARY KEY (`id`)	
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

-- ----------------------------------------------------------------------------------
-- 
-- CRIANDO A TABELA `login`
--
-- REPONSAVEL POR ARMAZENAR AS CREDENCIAIS PARA ACESSAR O SISTEMA
CREATE TABLE `login` 
(
	`id` BIGINT AUTO_INCREMENT,
	`usuario_id` BIGINT NOT NULL,
	`senha` VARCHAR(80) NOT NULL,
	`status` INT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

-- ----------------------------------------------------------------------------------
-- 
-- CRIANDO A TABELA `tipo_contato`
--
-- REPONSAVEL POR ARMAZENAR O TIPO DE CONTATO, PARA QUE POSSAR SER DIFERENCIADO CADA 
-- CONTATOS QUE ESTA ARMAZENADO
CREATE TABLE `tipo_contato`
(
	`id` BIGINT AUTO_INCREMENT,
	`desc` VARCHAR(355) NOT NULL,
	`status` INT NULL,
	PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

-- ----------------------------------------------------------------------------------
-- 
-- CRIANDO A TABELA `contato`
--
-- REPONSAVEL POR ARMAZENAR AS INFORMACOES DE CONTATO
CREATE TABLE `contato`
(
	`id` BIGINT AUTO_INCREMENT,
	`usuario_id` BIGINT NOT NULL,
	`tipo_contato_id` INT NOT NULL,
	`status` INT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`),
	FOREIGN KEY (`tipo_contato_id`) REFERENCES `tipo_contato` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;